const { getRooms, getRoomById } = require("./rooms");

/**
 * @param {import("express").Application} app
 */
function builderAppRoutes(app, port = 8080) {
  app.get("/rooms", getRooms);
  app.get("/rooms/:id", getRoomById);

  app.listen(port, () => {
    console.log("REST Server listen on port 8080");
  });
}

module.exports = {
  builderAppRoutes,
};
