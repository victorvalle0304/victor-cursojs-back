const URL = "https://rickandmortyapi.com/api/character/"
const LAST_PAGE = 34
const https = require("https");
const { LOAD_CHARACTERS, PICK_CHARACTER, GUESSING_CHARACTER } = require("../common/constants/events")


function getCharacters(page = 0) {
  

  return new Promise((resolve, reject) => {
    const req = https.get(`${URL}?page=${page}`, (resp) => {
      let data = '';

      // A chunk of data has been recieved.
      resp.on('data', (chunk) => {
        data += chunk;
      });
      // The whole response has been received. Print out the result.
      resp.on('end', () => {
        if (resp.statusCode >= 200 && resp.statusCode <= 299) {
          resolve(JSON.parse(data).results);
        } else {
          reject('Request failed. status: ' + resp.statusCode);
        }
      });
    }).on("error", (err) => {
      console.log("Error: " + err.message);
    });
  });
}

function getRandomCharacters() {
  const randomPage = Math.floor(Math.random() * LAST_PAGE)
  return getCharacters(randomPage)
    .then(data => data)
}

async function importCharacters() {
  const characters = await getRandomCharacters();
  return characters;
}

function loadCharacters(socket, characters) {
  socket.emit(LOAD_CHARACTERS, characters);
}

function sendPickCharacter(socket, characters, characterPicked) {
  socket.emit(PICK_CHARACTER, characters[characterPicked])
}

function guessingCharacter(socket, characterPicked) {
  socket.on(GUESSING_CHARACTER, (id) => {
    if (characterPicked.id === id) {
      console.log("You won")
    } else {
      console.log("You lost")
    }

  })
}



module.exports = {
  importCharacters,
  loadCharacters,
  sendPickCharacter,
  guessingCharacter,
};











