// const {
//   DISCONNECT,
//   EMIT_RESPONSE,
//   SEND_MESSAGE,
//   CREATE_USER,
// } = require("../common/constants/events");

// const { CLIENT_DISCONNECT } = require("../common/constants/successMessages");


// const io = require('socket.io')(3001);

// io.on('connection', socket => {
//   socket.emit('INIT','HOLA')
//   console.log('connected');
// });
const express = require("express");
const { createServer } = require("http");
const socketIo = require("socket.io");
const { socketHandlerBuilder } = require("./event-handlers");
const { builderAppRoutes } = require("./routes");

const port = process.env.PORT || 3001;

const app = express();
builderAppRoutes(app);

const server = createServer(app);
const io = socketIo(server);
socketHandlerBuilder(io);

server.listen(port, () => console.log(`Listening on port ${port}`));
