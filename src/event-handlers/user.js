const {
  DISCONNECT,
  EMIT_RESPONSE,
  SEND_MESSAGE,
  CREATE_USER,
} = require("../common/constants/events");
const { CLIENT_DISCONNECT } = require("../common/constants/successMessages");

function createUser(socket) {
  const NICK = Math.floor(Math.random() * 100);

  const toEmit = {
    nick: NICK,
    data: "",
  };
  socket.emit(CREATE_USER, JSON.stringify(toEmit));
}

function sendMessage(socket, global) {
  socket.on(SEND_MESSAGE, (message) => {
    global.emit(EMIT_RESPONSE, message);
  });
}

function disconnect(socket, global) {
  const ADMIN_NICK = "admin";
  socket.on(DISCONNECT, () => {
    const toEmit = {
      nick: ADMIN_NICK,
      data: `${CLIENT_DISCONNECT}: ${NICK}`,
    };
    global.emit(EMIT_RESPONSE, JSON.stringify(toEmit));
  });
}

module.exports = {
  createUser,
  sendMessage,
  disconnect,
};
