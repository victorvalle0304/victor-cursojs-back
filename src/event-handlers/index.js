const { createUser, sendMessage, disconnect, factory } = require("./user");
const { importCharacters, loadCharacters, sendPickCharacter, guessingCharacter } = require("../services/characters");
const { CONNECT } = require("../common/constants/events");
const { CLIENT_CONNECT } = require("../common/constants/successMessages");

function socketHandlerBuilder(socket) {
  socket.on(CONNECT, factoryConnect(socket));
}

function factoryConnect(global) {
  return (socket) => {
    connectHandler(socket, global);
  };
}

async function connectHandler(socket, global) {
  console.log(CLIENT_CONNECT);
  createUser(socket, global);  
  const characters = await importCharacters ();  
  const characterPicked = Math.floor(Math.random() * characters.length);
  loadCharacters(socket, characters);
  sendPickCharacter(socket, characters, characterPicked);
  guessingCharacter(socket, characters[characterPicked])
  sendMessage(socket, global);
  //disconnect(socket, global);
}

module.exports = {
  socketHandlerBuilder,
};
