module.exports = {
  CLIENT_CONNECT: "New client connected",
  CLIENT_DISCONNECT: "A client was disconnected",
};