module.exports = {
  CONNECT: "connection",
  CREATE_USER: "createUser",
  SEND_MESSAGE: "sendMessage",
  EMIT_RESPONSE: "emitResponse",
  DISCONNECT: "disconnect",
  LOAD_CHARACTERS: "loadCharacters",
  PICK_CHARACTER: "pickCharacter",
  GUESSING_CHARACTER: "guessingCharacter",
};