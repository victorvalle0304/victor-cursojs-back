/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 */
function getRooms(req, res) {
  const user = req.headers["user-agent"];
  res.statusCode = 200;
  res.send({ user });
}

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 */
function getRoomById(req, res) {
  const user = req.headers["user-agent"];
  res.statusCode = 404;
  res.send({ error: "Not found" });
}

module.exports = {
  getRooms,
  getRoomById,
};
